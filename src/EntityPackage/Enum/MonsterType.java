package EntityPackage.Enum;

public enum MonsterType
{
    NONE,
    NORMAL,
    ROBOT,
    ANDROID,
    UNDEAD,
    MERCENARY,
    INSECT,
    GATE_KEEPER,
    SUPER_NATURAL,
    DRAGON,
    MAGIC
}
