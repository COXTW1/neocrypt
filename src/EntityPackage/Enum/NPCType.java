package EntityPackage.Enum;

public enum NPCType
{
    NONE,
    VILLAGER,
    TRAVELOR,
    KID,
    VENDOR,
    PRIEST,
    DOCTOR,
    MERCENARY
}
