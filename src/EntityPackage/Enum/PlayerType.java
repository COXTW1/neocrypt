package EntityPackage.Enum;

public enum PlayerType
{
    NONE,
    BRAWLER,
    SWORD_FIGHTER,
    GUNNER,
    SPHERE_FIGHTER,
    DEFENDER,
    MAGIC_USER
}
