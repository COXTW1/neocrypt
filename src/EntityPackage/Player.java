package EntityPackage;

import EntityPackage.Enum.*;
import GamePackage.Stat.Stats;
import WeaponPackage.*;

public class Player extends Entity
{
    public Player()
    {
        super();

        setPlayerType(PlayerType.BRAWLER);
    }

    public Player(boolean armed, PlayerType playerType, Weapon weapon)
    {
        super(armed, MonsterType.NONE, playerType, NPCType.NONE, weapon);

        if(playerType == PlayerType.NONE)
        {
            setPlayerType(PlayerType.BRAWLER);
        }
    }

    public Player(Stats inStat, boolean armed, PlayerType playerType, Weapon weapon)
    {
        super(inStat, armed, MonsterType.NONE, playerType, NPCType.NONE, weapon);

        if(playerType == PlayerType.NONE)
        {
            setPlayerType(PlayerType.BRAWLER);
        }
    }

    public Player(boolean armed, PlayerType playerType, Weapon weapon, Stats inStat)
    {
        super(armed, MonsterType.NONE, playerType, weapon, NPCType.NONE, inStat);

        if(playerType == PlayerType.NONE)
        {
            setPlayerType(PlayerType.BRAWLER);
        }
    }

    public Player(Player player)
    {
        super(player.getArmed(), player.getMonsterType(), player.getPlayerType(),
                player.getWeapon(), player.getNpcType(), player.getStat());

        if(player.getPlayerType() == PlayerType.NONE)
        {
            setPlayerType(PlayerType.BRAWLER);
        }
    }
}