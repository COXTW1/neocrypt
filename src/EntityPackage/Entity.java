package EntityPackage;

import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Enum.*;
//import GamePackage.Stat.*;
import GamePackage.Stat.*;
import ItemPackage.*;
import ItemPackage.Enum.*;
import WeaponPackage.*;
import WeaponPackage.Enum.*;

public abstract class Entity
{
    private final Stats DEFAULT_STAT = new Stats();

    private MonsterType monsterType;
    private PlayerType playerType;
    private WeaponType weaponType;
    private ItemType itemtype;
    private NPCType npcType;
    private boolean armed;
    private Weapon weapon;
    private Stats stat;

    public StatInjector statInjector;


    public Entity()
    {
        setArmed(false);
        setMonsterType(MonsterType.NONE);
        setPlayerType(PlayerType.NONE);
        setNPCType(NPCType.NONE);
        setWeapon(new Weapon());

        setStat(DEFAULT_STAT);

        this.statInjector = new StatInjector();
        //this.statInjector = new StatInjector();
    }

    public Entity(boolean armed, MonsterType monsterType, PlayerType playerType, NPCType npcType, Weapon weapon)
    {
        setArmed(armed);
        setMonsterType(monsterType);
        setPlayerType(playerType);
        setNPCType(npcType);
        setWeapon(weapon);

        this.statInjector = new StatInjector();

        setStat(DEFAULT_STAT);
    }

    public Entity(Stats inStat, boolean armed, MonsterType monsterType, PlayerType playerType, NPCType npcType, Weapon weapon)
    {
        setArmed(armed);
        setMonsterType(monsterType);
        setPlayerType(playerType);
        setNPCType(npcType);
        setWeapon(weapon);

        this.statInjector = new StatInjector();

        setStat(inStat);
    }

    public Entity(boolean armed, MonsterType monsterType, PlayerType playerType, Weapon weapon, NPCType npcType, Stats inStat)
    {
        setArmed(armed);
        setMonsterType(monsterType);
        setPlayerType(playerType);
        setNPCType(npcType);
        setWeapon(weapon);

        this.statInjector = new StatInjector();

        setStat(inStat);
    }

    public void setMonsterType(MonsterType monsterType)
    {
        this.monsterType = monsterType;
    }

    public void setPlayerType(PlayerType playerType)
    {
        this.playerType = playerType;
    }

    public void setWeapon(Weapon weapon)
    {
        this.weapon = new Weapon(weapon);
    }

    public void setArmed(boolean armed)
    {
        this.armed = armed;
    }

    public boolean getArmed()
    {
        return armed;
    }

    public MonsterType getMonsterType()
    {
        return monsterType;
    }

    public PlayerType getPlayerType()
    {
        return playerType;
    }

    public NPCType getNpcType() {
        return npcType;
    }

    public void setNPCType(NPCType npcType) {
        this.npcType = npcType;
    }

    public Weapon getWeapon()
    {
        return weapon;
    }

    public void setStat(Stats inStat)
    {
        this.stat = new Stats(inStat);
    }

    public Stats getStat()
    {
        return stat;
    }

    public class StatInjector
    {
        //private Stats outStat;
        private Stats outStat;

        public StatInjector()
        {
            Stats newStat = new Stats();

            setOutStat(newStat);
        }

        public StatInjector(Stats inStat)
        {
            Stats newStat = new Stats(inStat);

            setOutStat(newStat);
        }

        public Stats getOutStat()
        {
            Stats oStat = new Stats(this.outStat);

            return oStat;
        }

        public void setOutStat(Stats inStat)
        {
            this.outStat = new Stats(inStat);
        }

        public void inject(Stats inStat, double health, double armor, double attack,
                               double defense, double fatigue, double speed, double stamina) throws Exception
        {
            setOutStat(inStat);

            if(health != 0.0)
            {
                this.outStat.setHealth(health);
            }

            if(armor != 0.0)
            {
                this.outStat.setArmor(armor);
            }

            if(attack != 0.0)
            {
                this.outStat.setAttack(attack);
            }

            if(defense != 0.0)
            {
                this.outStat.setDefense(defense);
            }

            if(fatigue != 0.0)
            {
                this.outStat.setFatigue(fatigue);
            }

            if(speed != 0.0)
            {
                this.outStat.setSpeed(speed);
            }

            if(stamina != 0.0)
            {
                this.outStat.setStamina(stamina);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(double[] newStats, Stats inStat) throws Exception
        {
            setOutStat(inStat);

            if(newStats[0] != 0.0)
            {
                this.outStat.setHealth(newStats[0]);
            }

            if(newStats[1] != 0.0)
            {
                this.outStat.setArmor(newStats[1]);
            }

            if(newStats[2] != 0.0)
            {
                this.outStat.setAttack(newStats[2]);
            }

            if(newStats[3] != 0.0)
            {
                this.outStat.setDefense(newStats[3]);
            }

            if(newStats[4] != 0.0)
            {
                this.outStat.setFatigue(newStats[4]);
            }

            if(newStats[5] != 0.0)
            {
                this.outStat.setSpeed(newStats[5]);
            }

            if(newStats[6] != 0.0)
            {
                this.outStat.setStamina(newStats[6]);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(double health, double armor, double attack,
                               double defense, double fatigue, double speed, double stamina, Stats inStat) throws Exception
        {
            setOutStat(inStat);

            if(health != 0.0)
            {
                this.outStat.setHealth(health);
            }

            if(armor != 0.0)
            {
                this.outStat.setArmor(armor);
            }

            if(attack != 0.0)
            {
                this.outStat.setAttack(attack);
            }

            if(defense != 0.0)
            {
                this.outStat.setDefense(defense);
            }

            if(fatigue != 0.0)
            {
                this.outStat.setFatigue(fatigue);
            }

            if(speed != 0.0)
            {
                this.outStat.setSpeed(speed);
            }

            if(stamina != 0.0)
            {
                this.outStat.setStamina(stamina);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(Stats inStat, double[] newStats) throws Exception
        {
            setOutStat(inStat);

            if(newStats[0] != 0.0)
            {
                this.outStat.setHealth(newStats[0]);
            }

            if(newStats[1] != 0.0)
            {
                this.outStat.setArmor(newStats[1]);
            }

            if(newStats[2] != 0.0)
            {
                this.outStat.setAttack(newStats[2]);
            }

            if(newStats[3] != 0.0)
            {
                this.outStat.setDefense(newStats[3]);
            }

            if(newStats[4] != 0.0)
            {
                this.outStat.setFatigue(newStats[4]);
            }

            if(newStats[5] != 0.0)
            {
                this.outStat.setSpeed(newStats[5]);
            }

            if(newStats[6] != 0.0)
            {
                this.outStat.setStamina(newStats[6]);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(Stats inStat) throws Exception
        {
            setOutStat(inStat);

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(double health, double armor, double attack,
                               double defense, double fatigue, double speed, double stamina) throws Exception
        {
            Stats blankStat = new Stats();

            setOutStat(blankStat);

            if(health != 0.0)
            {
                this.outStat.setHealth(health);
            }

            if(armor != 0.0)
            {
                this.outStat.setArmor(armor);
            }

            if(attack != 0.0)
            {
                this.outStat.setAttack(attack);
            }

            if(defense != 0.0)
            {
                this.outStat.setDefense(defense);
            }

            if(fatigue != 0.0)
            {
                this.outStat.setFatigue(fatigue);
            }

            if(speed != 0.0)
            {
                this.outStat.setSpeed(speed);
            }

            if(stamina != 0.0)
            {
                this.outStat.setStamina(stamina);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }

        public void inject(double[] newStats) throws Exception
        {
            Stats blankStat = new Stats();

            setOutStat(blankStat);

            if(newStats[0] != 0.0)
            {
                this.outStat.setHealth(newStats[0]);
            }

            if(newStats[1] != 0.0)
            {
                this.outStat.setArmor(newStats[1]);
            }

            if(newStats[2] != 0.0)
            {
                this.outStat.setAttack(newStats[2]);
            }

            if(newStats[3] != 0.0)
            {
                this.outStat.setDefense(newStats[3]);
            }

            if(newStats[4] != 0.0)
            {
                this.outStat.setFatigue(newStats[4]);
            }

            if(newStats[5] != 0.0)
            {
                this.outStat.setSpeed(newStats[5]);
            }

            if(newStats[6] != 0.0)
            {
                this.outStat.setStamina(newStats[6]);
            }

            //Entity.this.setStat(outStat);

            Entity.this.stat = outStat;
        }


        public String toString()
        {
            String out;

            out = this.outStat.toString();

            return out;
        }
    }
}
