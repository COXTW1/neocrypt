package EntityPackage;

import EntityPackage.Enum.*;
import GamePackage.Stat.Stats;
import WeaponPackage.*;

public class NPC extends Entity
{
    public NPC()
    {
        super();

        setNPCType(NPCType.TRAVELOR);
    }

    public NPC(boolean armed, NPCType npcType, Weapon weapon)
    {
        super(armed, MonsterType.NONE, PlayerType.NONE, npcType, weapon);

        if(npcType == NPCType.NONE)
        {
            setNPCType(NPCType.TRAVELOR);
        }
    }

    public NPC(Stats inStat, boolean armed, NPCType npcType, Weapon weapon)
    {
        super(inStat, armed, MonsterType.NONE, PlayerType.NONE, npcType, weapon);

        if(npcType == NPCType.NONE)
        {
            setNPCType(NPCType.TRAVELOR);
        }
    }

    public NPC(boolean armed, NPCType npcType, Weapon weapon, Stats inStat)
    {
        super(armed, MonsterType.NONE, PlayerType.NONE, weapon, npcType, inStat);

        if(npcType == NPCType.NONE)
        {
            setNPCType(NPCType.TRAVELOR);
        }
    }

    public NPC(NPC npc)
    {
        super(npc.getArmed(), npc.getMonsterType(), npc.getPlayerType(),
                npc.getWeapon(), npc.getNpcType(), npc.getStat());

        if(npc.getNpcType() == NPCType.NONE)
        {
            setNPCType(NPCType.TRAVELOR);
        }
    }
}
