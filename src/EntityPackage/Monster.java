package EntityPackage;

import EntityPackage.Enum.*;
import GamePackage.Stat.Stats;
import WeaponPackage.*;

public class Monster extends Entity
{
    public Monster()
    {
        super();

        setMonsterType(MonsterType.NORMAL);
    }

    public Monster(boolean armed, MonsterType monsterType, Weapon weapon)
    {
        super(armed, monsterType, PlayerType.NONE, NPCType.NONE, weapon);

        if(monsterType == MonsterType.NONE)
        {
            setMonsterType(MonsterType.NORMAL);
        }
    }

    public Monster(Stats inStat, boolean armed, MonsterType monsterType, Weapon weapon)
    {
        super(inStat, armed, monsterType, PlayerType.NONE, NPCType.NONE, weapon);

        if(monsterType == MonsterType.NONE)
        {
            setMonsterType(MonsterType.NORMAL);
        }
    }

    public Monster(boolean armed, MonsterType monsterType, Weapon weapon, Stats inStat)
    {
        super(armed, monsterType, PlayerType.NONE, weapon, NPCType.NONE, inStat);

        if(monsterType == MonsterType.NONE)
        {
            setMonsterType(MonsterType.NORMAL);
        }
    }

    public Monster(Monster monster)
    {
        super(monster.getArmed(), monster.getMonsterType(), monster.getPlayerType(),
                monster.getWeapon(), monster.getNpcType(), monster.getStat());

        if(monster.getMonsterType() == MonsterType.NONE)
        {
            setMonsterType(MonsterType.NORMAL);
        }
    }
}
