package GamePackage.Stat;

public class Stats
{
    private double health;
    private double armor;
    private double attack;
    private double defense;
    private double fatigue;
    private double speed;
    private double stamina;

    public Stats()
    {
        setHealth(10);
        setArmor(0.0);
        setAttack(1.0);
        setDefense(1.0);
        setFatigue(0.0);
        setSpeed(1.0);
        setStamina(1.0);
    }

    public Stats(Stats inStat)
    {
        setHealth(inStat.getHealth());
        setArmor(inStat.getArmor());
        setAttack(inStat.getAttack());
        setDefense(inStat.getDefense());
        setFatigue(inStat.getFatigue());
        setSpeed(inStat.getSpeed());
        setStamina(inStat.getStamina());
    }

    public Stats(double health, double armor, double attack, double defense,
                 double fatigue, double speed, double stamina)
    {
        setHealth(health);
        setArmor(armor);
        setAttack(attack);
        setDefense(defense);
        setFatigue(fatigue);
        setSpeed(speed);
        setStamina(stamina);
    }

    public double getHealth()
    {
        return health;
    }

    public void setHealth(double health)
    {
        this.health = health;
    }

    public double getArmor()
    {
        return armor;
    }

    public void setArmor(double armor)
    {
        this.armor = armor;
    }

    public double getAttack()
    {
        return attack;
    }

    public void setAttack(double attack)
    {
        this.attack = attack;
    }

    public double getDefense()
    {
        return defense;
    }

    public void setDefense(double defense)
    {
        this.defense = defense;
    }

    public double getFatigue()
    {
        return fatigue;
    }

    public void setFatigue(double fatigue)
    {
        this.fatigue = fatigue;
    }

    public double getSpeed()
    {
        return speed;
    }

    public void setSpeed(double speed)
    {
        this.speed = speed;
    }

    public double getStamina()
    {
        return stamina;
    }

    public void setStamina(double stamina)
    {
        this.stamina = stamina;
    }

    @Override
    public String toString()
    {
        String out;

        out =   "\n" +
                "Here are the Stats: \n" +
                "\n health=" + health +
                "\n armor=" + armor +
                "\n attack=" + attack +
                "\n defense=" + defense +
                "\n fatigue=" + fatigue +
                "\n speed=" + speed +
                "\n stamina=" + stamina +
                "\n";

        return out;
    }
}
