package GamePackage;

//import EntityPackage.*;
import EntityPackage.*;
import GamePackage.Enum.*;
import GamePackage.Stat.*;
import WorldBuilderPackage.Two_Dimensional_World;
import WorldBuilderPackage.World;

public class Game
{
    private World world;
    private Direction direction;
    private Entity[] players;
    private Entity[] monsters;
    private Entity[] npcs;

    public Game()
    {
        this.world = new Two_Dimensional_World();
        this.direction = Direction.CENTER;
        this.players = new Player[1];
        this.monsters = new Monster[10];
        this.npcs = new NPC[10];
    }

    public Game(Two_Dimensional_World world, Direction direction, Player player, Monster monster, NPC npc)
    {
        this.world = new Two_Dimensional_World(world);
        this.direction = direction;
        this.players = new Player[1];
        this.monsters = new Monster[10];
        this.npcs = new NPC[10];
    }

    public Game(World world, Direction direction)
    {
        this.world = new Two_Dimensional_World(world);
        this.direction = direction;
        this.players = new Player[1];
        this.monsters = new Monster[10];
        this.npcs = new NPC[10];
    }

    public Game(Stats stats, Player player, Monster monster, NPC npc)
    {
        this.world = new Two_Dimensional_World();
        this.direction = Direction.CENTER;
        this.players = new Player[1];
        this.monsters = new Monster[10];
        this.npcs = new NPC[10];
    }

    public World getWorld()
    {
        return this.world;
    }

    public void setWorld(Two_Dimensional_World world)
    {
        this.world = new Two_Dimensional_World(world);
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }

    public Entity[] getPlayers()
    {
        return this.players;
    }

    public void setPlayers(Player[] inPlayers)
    {
        this.players = new Entity[inPlayers.length];

        for(int x = 0; x < this.players.length; x++)
        {
            this.players[x] = new Player(inPlayers[x]);
        }
    }

    public Entity[] getMonsters()
    {
        return this.monsters;
    }

    public void setMonsters(Monster[] inMonsters)
    {
        this.monsters = new Entity[inMonsters.length];

        for(int x = 0; x < this.monsters.length; x++)
        {
            this.monsters[x] = new Monster(inMonsters[x]);
        }
    }

    public Entity[] getNpcs()
    {
        return this.npcs;
    }

    public void setNpcs(NPC[] npcs)
    {
        this.npcs = new Entity[npcs.length];

        for(int x = 0; x < this.npcs.length; x++)
        {
            this.npcs[x] = new NPC(npcs[x]);
        }
    }
}
