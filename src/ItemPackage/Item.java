package ItemPackage;

import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Enum.*;
import GamePackage.Stat.*;
import ItemPackage.*;
import ItemPackage.Enum.*;
import WeaponPackage.*;
import WeaponPackage.Enum.*;

public abstract class Item
{
    private ItemType type;

    public Item()
    {
        setType(ItemType.NONE);
    }

    public Item(ItemType type)
    {
        setType(type);
    }

    public ItemType getType()
    {
        return this.type;
    }

    public void setType(ItemType type)
    {
        this.type = type;
    }
}
