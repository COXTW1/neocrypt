package ItemPackage.Enum;

public enum ItemType
{
    NONE,
    COMPASS,
    HEALTH_POTION,
    STAMINA_POTION,
    LEAF
}
