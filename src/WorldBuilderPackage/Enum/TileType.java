package WorldBuilderPackage.Enum;

public enum TileType
{
    NONE,
    PATH,
    VERT_WALL,
    HORIZ_WALL,
    ROCK,
    ITEM
}
