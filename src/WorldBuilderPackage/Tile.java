package WorldBuilderPackage;

import java.util.*;

import WorldBuilderPackage.Enum.TileType;

public class Tile
{
    private boolean movable;
    private boolean object;
    private boolean interactable;
    private TileType type;
    private ASCI_Tile asci_tile;

    public Tile()
    {
        setObject(false);
        setType(TileType.NONE);
        setInteractable(false);
        setMovable(true);
    }

    public Tile(TileType tileType)
    {
        if(tileType == TileType.HORIZ_WALL)
        {
            setObject(false);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.VERT_WALL)
        {
            setObject(false);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.PATH)
        {
            setObject(false);
            setInteractable(false);
            setMovable(true);
        }
        else if(tileType == TileType.ROCK)
        {
            setObject(true);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.ITEM)
        {
            setObject(true);
            setInteractable(true);
            setMovable(true);
        }
        else
        {
            setObject(false);
            setInteractable(false);
            setMovable(true);
        }

        setType(tileType);
    }

    public Tile(TileType tileType, boolean interactable)
    {
        if(tileType == TileType.HORIZ_WALL)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(false);
        }
        else if(tileType == TileType.VERT_WALL)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(false);
        }
        else if(tileType == TileType.PATH)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(true);
        }
        else if(tileType == TileType.ROCK)
        {
            setObject(true);
            setInteractable(interactable);
            setMovable(false);
        }
        else if(tileType == TileType.ITEM)
        {
            setObject(true);
            setInteractable(interactable);
            setMovable(true);
        }
        else
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(true);
        }

        setType(tileType);
    }

    public Tile(TileType tileType, boolean interactable, boolean movable)
    {
        if(tileType == TileType.HORIZ_WALL)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(false);
        }
        else if(tileType == TileType.VERT_WALL)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(false);
        }
        else if(tileType == TileType.PATH)
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(true);
        }
        else if(tileType == TileType.ROCK)
        {
            setObject(true);
            setInteractable(interactable);
            setMovable(movable);
        }
        else if(tileType == TileType.ITEM)
        {
            setObject(true);
            setInteractable(interactable);
            setMovable(movable);
        }
        else
        {
            setObject(false);
            setInteractable(interactable);
            setMovable(true);
        }

        setType(tileType);
    }


    public Tile(Tile inTile)
    {
        TileType tileType = inTile.getType();

        if(tileType == TileType.HORIZ_WALL)
        {
            setObject(false);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.VERT_WALL)
        {
            setObject(false);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.PATH)
        {
            setObject(false);
            setInteractable(false);
            setMovable(true);
        }
        else if(tileType == TileType.ROCK)
        {
            setObject(true);
            setInteractable(false);
            setMovable(false);
        }
        else if(tileType == TileType.ITEM)
        {
            setObject(true);
            setInteractable(true);
            setMovable(true);
        }
        else
        {
            setObject(false);
            setInteractable(false);
            setMovable(true);
        }

        setType(tileType);
    }

    public boolean getMovable()
    {
        return this.movable;
    }

    public void setMovable(boolean movable)
    {
        if(this.object == true)
        {
            this.movable = false;
        }
        else
        {
            this.movable = movable;
        }
    }

    public boolean getObject()
    {
        return this.object;
    }

    public void setObject(boolean object)
    {
        this.object = object;
    }

    public boolean getInteractable()
    {
        return this.interactable;
    }

    public void setInteractable(boolean interactable)
    {
        if(this.object == false)
        {
            this.interactable = false;
        }
        else
        {
            this.interactable = interactable;
        }
    }

    public TileType getType()
    {
        return this.type;
    }

    public void setType(TileType type)
    {
        this.type = type;

        setAsci_tile(this.type);
    }

    /*
    public void setAsci_tile(String tileType)
    {
        String tType = tileType.toLowerCase(); //converts inputted String to lower case

        if(tType == " " ||
            tType == "default")
        {
            this.asci_tile = new ASCI_Tile(); //calls default constructor for asci_tile
        }
        else
        {
            this.asci_tile = new ASCI_Tile(tileType); //sets the ASCI_Tile type based on passed String
        }
    }
    */

    public void setAsci_tile(TileType type)
    {
        this.asci_tile = new ASCI_Tile(type); //sets the ASCI_Tile Type based on passed TileType enum
    }

    /*
    public void setAsci_tile(ASCI_Tile inATile)
    {
        this.asci_tile = new ASCI_Tile(inATile); //sets the ASCI_Tile Type based on passed ASCI_Tile object
    }
    */

    public ASCI_Tile getAsci_tile()
    {
        return this.asci_tile; //returns asci_tile
    }

    public String getAsci_tileStr()
    {
        String output = this.asci_tile.getTile();

        return output;
    }

    public void setUp()
    {
        Scanner kb = new Scanner(System.in);
        String input;

        System.out.println("\n\n" +
                           "----------------------------------------------------------------------" + "\n\n" +
                            "Tile Setup activated (Press Enter or Return to continue)\n");

        kb.nextLine();


        //gets object status
        System.out.println("\nIs this tile an object?: (Yes or no)");
        input = kb.nextLine().toLowerCase();

        if(input.charAt(0) == 'y' || input.equals("true"))
        {
            setObject(true);
        }
        else
        {
            setObject(false);
        }

        //gets interactable status
        System.out.println("\nIs this tile interactable?: (Yes or no)");
        input = kb.nextLine().toLowerCase();

        if(input.charAt(0) == 'y' || input.equals("true"))
        {
            setInteractable(true);
        }
        else
        {
            setInteractable(false);
        }

        //gets object status
        System.out.println("\nCan you move into the object?: (Yes or no)");
        input = kb.nextLine().toLowerCase();

        if(input.charAt(0) == 'y' || input.equals("true"))
        {
            setMovable(true);
        }
        else
        {
            setMovable(false);
        }

        //gets object type
        if(this.object == true)
        {
            System.out.println("\nWhat type of object is it? " +
                    "\n\t1. Vertical Wall" +
                    "\n\t2. Horizontal Wall" +
                    "\n\t3. Rock" +
                    "\n\t4. Item" +
                    "\n");

            input = kb.nextLine().toLowerCase();

            if(input.equals("vertical") || input.equals("vert") ||
                    input.equals("verticalwall") || input.equals("vertwall") ||
                    input.equals("vertical_wall") || input.equals("vert_wall") ||
                    input.equals("vertical-wall") || input.equals("vert-wall") ||
                    input.equals("vertical wall") || input.equals("vert wall") ||
                    input.equals("vwall") || input.equals("v_wall") ||
                    input.equals("v-wall") || input.equals("v wall") ||
                    input.equals("v") ||
                    input.equals("1") || input.equals("1."))
            {
                setType(TileType.VERT_WALL);
            }
            else if(input.equals("horizontal") || input.equals("horiz") ||
                        input.equals("horizontalwall") || input.equals("horizwall") ||
                        input.equals("horizontal_wall") || input.equals("horiz_wall") ||
                        input.equals("horizontal-wall") || input.equals("horiz-wall") ||
                        input.equals("horizontal wall") || input.equals("horiz wall") ||
                        input.equals("hwall") || input.equals("h_wall") ||
                        input.equals("h-wall") || input.equals("h wall") ||
                        input.equals("h") ||
                        input.equals("2") || input.equals("2."))
            {
                setType(TileType.HORIZ_WALL);
            }
            else if(input.equals("rock") || input.equals("3") || input.equals("3."))
            {
                setType(TileType.ROCK);
            }
            else if(input.equals("item") || input.equals("4") || input.equals("4."))
            {
                setType(TileType.ITEM);
            }
        }
        else
        {
            setType(TileType.NONE);
        }

        System.out.println("\n\n" +
                            "Tile created! (Press Enter or Return to continue)" +
                            "----------------------------------------------------------------------");

        kb.nextLine();

    }

    @Override
    public String toString() {
        return "Tile{" +
                "movable= " + this.movable +
                ", object= " + this.object +
                ", interactable= " + this.interactable +
                ", type= " + this.type +
                ", tile value= " + this.asci_tile.getTile() +
                '}';
    }
}
