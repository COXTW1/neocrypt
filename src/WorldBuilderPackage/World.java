package WorldBuilderPackage;

import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Enum.*;
import GamePackage.Stat.*;
import ItemPackage.*;
import ItemPackage.Enum.*;
import WeaponPackage.*;
import WeaponPackage.Enum.*;

public abstract class World
{
    protected final int X_START = 0;
    protected final int Y_START = 0;
    protected final int Z_START = 0;

    protected int X_Pos;
    protected int Y_Pos;
    protected int Z_Pos;

    public World()
    {
        this.X_Pos = X_START;
        this.Y_Pos = Y_START;
        this.Z_Pos = Z_START;
    }

    public World(int x, int y, int z)
    {
        this.X_Pos = x;
        this.Y_Pos = y;
        this.Z_Pos = z;
    }

    public World(int x, int y)
    {
        this.X_Pos = x;
        this.Y_Pos = y;
        this.Z_Pos = Z_START;
    }

    public World(World world)
    {
        this.X_Pos = world.X_Pos;
        this.Y_Pos = world.Y_Pos;
        this.Z_Pos = world.Z_Pos;
    }

    public int getX_Pos()
    {
        return X_Pos;
    }

    public void setX_Pos(int x_Pos)
    {
        X_Pos = x_Pos;
    }

    public int getY_Pos()
    {
        return Y_Pos;
    }

    public void setY_Pos(int y_Pos)
    {
        Y_Pos = y_Pos;
    }

    public int getZ_Pos()
    {
        return Z_Pos;
    }

    public void setZ_Pos(int z_Pos)
    {
        Z_Pos = z_Pos;
    }

    @Override
    public String toString()
    {
        String out;

        out = "\nx coordinate: " + this.X_Pos +
                "\ny coordinate: " + this.Y_Pos +
                "\nz coordinate: " + this.Z_Pos;

        return out;
    }
}
