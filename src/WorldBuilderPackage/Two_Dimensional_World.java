package WorldBuilderPackage;

import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Enum.*;
import GamePackage.Stat.*;
import ItemPackage.*;
import ItemPackage.Enum.*;
import WeaponPackage.*;
import WeaponPackage.Enum.*;
import WorldBuilderPackage.Enum.TileType;

import java.util.Scanner;

public class Two_Dimensional_World extends World
{
    private Tile[][] grid;

    //used for testing
    private final Scanner kb = new Scanner(System.in);

    public Two_Dimensional_World()
    {
        super();

        Tile[][] newGrid = new Tile[5][5];

        for(int x = 0; x < newGrid.length; x++)
        {
            for(int y = 0; y < newGrid[x].length; y++)
            {
                newGrid[x][y] = new Tile();
            }
        }

        setGrid(newGrid);
    }

    public Two_Dimensional_World(int xIn, int yIn)
    {
        super(xIn, yIn);

        Tile[][] newGrid = new Tile[5][5];

        for(int x = 0; x < newGrid.length; x++)
        {
            for(int y = 0; y < newGrid[x].length; y++)
            {
                newGrid[x][y] = new Tile();
            }
        }

        setGrid(newGrid);
    }

    public Two_Dimensional_World(Tile[][] newGrid, World world)
    {
        super(world);

        setGrid(newGrid);
    }

    public Two_Dimensional_World(World world, Tile[][] newGrid)
    {
        super(world);

        setGrid(newGrid);
    }

    public Two_Dimensional_World(World world)
    {
        super(world);

        setGrid(new Tile[5][5]);
    }

    public Two_Dimensional_World(Two_Dimensional_World world)
    {
        super(world);

        setGrid(world.getGrid());
    }

    public Tile[][] getGrid()
    {
        return this.grid;
    }

    public void setGrid(Tile[][] newGrid)
    {
        this.grid = new Tile[newGrid.length][];

        for(int x = 0; x < newGrid.length; x++)
        {
            this.grid[x] = new Tile[newGrid[x].length];

            for(int y = 0; y < newGrid[x].length; y++)
            {
                this.grid[x][y] = newGrid[x][y];

                Tile tileType = newGrid[x][y];

                /*
                System.out.print(tileType + " at cor x: " + x + " y: " + y + "\n--------------------\n");

                kb.nextLine();
                */

                //this.grid[x][y] = new Tile(newGrid[x][y].getType());
            }
        }
    }

    public String build()
    {
        String out;                             //output string
        String nextTile;                        //String value for the next tile

        out = "";

        for(int x = 0; x < this.grid.length; x++)
        {
            for(int y = 0; y < this.grid[x].length; y++)
            {
                nextTile = this.grid[x][y].getAsci_tileStr();   //sets value of the next Tile

                out = out + nextTile;    //concatinates output String
            }

            out = out + "\n";                       //appends new line to output String
        }

        return out;                                 //returns output String
    }


    @Override
    public String toString()
    {
        String out;

        out = "\nx coordinate: " + this.X_Pos +
                "\ny coordinate: " + this.Y_Pos;

        return out;
    }
}
