package WorldBuilderPackage;

import WorldBuilderPackage.Enum.TileType;

public class ASCI_Tile
{
    private String tile;

    public ASCI_Tile()
    {
        setTile(TileType.NONE);
    }

    public ASCI_Tile(TileType type)
    {
        setTile(type);
    }

    public ASCI_Tile(String type)
    {
        setTile(type);
    }

    public ASCI_Tile(ASCI_Tile inTile)
    {
        setTile(inTile.getTile());
    }

    public String getTile()
    {
        return this.tile;
    }

    public void setTile(String type)
    {
        String input = type.toLowerCase();
        //TileType tileSet = TileType.NONE;
        TileType tileSet;

        if(input.equals("vertical") || input.equals("vert") ||
                input.equals("verticalwall") || input.equals("vertwall") ||
                input.equals("vertical_wall") || input.equals("vert_wall") ||
                input.equals("vertical-wall") || input.equals("vert-wall") ||
                input.equals("vertical wall") || input.equals("vert wall") ||
                input.equals("vwall") || input.equals("v_wall") ||
                input.equals("v-wall") || input.equals("v wall") ||
                input.equals("v"))
        {
            tileSet = TileType.VERT_WALL;
        }
        else if(input.equals("horizontal") || input.equals("horiz") ||
                    input.equals("horizontalwall") || input.equals("horizwall") ||
                    input.equals("horizontal_wall") || input.equals("horiz_wall") ||
                    input.equals("horizontal-wall") || input.equals("horiz-wall") ||
                    input.equals("horizontal wall") || input.equals("horiz wall") ||
                    input.equals("hwall") || input.equals("h_wall") ||
                    input.equals("h-wall") || input.equals("h wall") ||
                    input.equals("h"))
        {
            tileSet = TileType.HORIZ_WALL;
        }
        else if(input.equals("rock") || input.equals("@"))
        {
            tileSet = TileType.ROCK;
        }
        else if(input.equals("item") || input.equals("."))
        {
            tileSet = TileType.ITEM;
        }
        else if(input.equals("path") || input.equals("#"))
        {
            tileSet = TileType.PATH;
        }
        else
        {
            tileSet = TileType.NONE;
        }

        setTile(tileSet);
    }

    public void setTile(TileType type)
    {
        if(type == TileType.VERT_WALL)
        {
            this.tile = "H";
        }
        else if(type == TileType.HORIZ_WALL)
        {
            this.tile = "---";
        }
        else if(type == TileType.ROCK)
        {
            this.tile = " @ ";
        }
        else if(type == TileType.ITEM)
        {
            this.tile = " . ";
        }
        else if (type == TileType.PATH)
        {
            this.tile = " # ";
        }
        else
        {
            this.tile = "   ";
        }
    }

    public void setCustChar(String in)
    {
        char charInput = in.toLowerCase().charAt(0);
        String input = "" + charInput;

        this.tile = input;
    }

    public void setCustChar(char in)
    {
        String input = "" + in;

        this.tile = input;
    }
}
