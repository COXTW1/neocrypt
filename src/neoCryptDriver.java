
import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Stat.*;
import WeaponPackage.*;
import WorldBuilderPackage.*;
import WorldBuilderPackage.Enum.TileType;

import java.util.*;

public class neoCryptDriver
{
    //Constants
    private static final Scanner KB = new Scanner(System.in);
    private static final int INITIAL_LEN_WID = 10;
    private static final String NOT_IMPLEMENTED_MESSAGE = "This feature is not yet implemented.";
    private static final String WELCOME_MESSAGE = "Welcome to NeoCrpyt!";
    private static final String CONTINUE = "<<Please press Enter/Return to continue!>>";
    private static final String ERROR = "Not a valid input!";
    private static final String CLEAR = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    private static final String SKIP_LINES = "\n\n\n\n";
    private static final String BORDER = "\n----------------------------------------\n";
    private static final String GOODBYE_MESSAGE = "Thank you for playing!";
    private static final String ABOUT_MESSAGE = "\n\n" +
            BORDER + "\n" +
            "\tThis game is in pre-Alpha development. \n" +
            "NeoCrypt is a text adventure game that\n" +
            "utilizes elements of science fiction and\n" +
            "fantasy.\n" +
            BORDER +
            "\n\n";
    private static final String MENU = "\n\n" +
            BORDER +
            "\n\n" +
            "In order to proceed, either Press a button in the window,\n" +
            "type the menu option below, or Enter the number of the \n" +
            "corresponding menu item below: \n\n" +
            "1. Go NORTH (UP)\n" +
            "2. Go SOUTH (DOWN)\n" +
            "3. Go WEST (LEFT)\n" +
            "4. Go EAST (RIGHT)\n" +
            "5. Clear the screen\n" +
            "6. Check your status\n" +
            "7. Exit the Game\n" +
            "8. About the game\n" +
            "\n" + BORDER +
            "\n\n";

    //non-Constants
    private static Game game = new Game();
    //private static Entity default_Entity;
    private static Player[] players = new Player[1];
    private static Weapon[] playerWeapons = new Weapon[1];
    private static Monster[] monsters = new Monster[3];
    private static Weapon[] monsterWeapons = new Weapon[2];

    //Sets player 1 stats
    private static Stats playerStat;

    private static boolean repeat = true;
    private static String input = "";
    private static String out = "";
    private static char charInput = ' ';


    /**
     * main method of driver
     *
     * accepts: args - an array of Strings
     *
     * returns: void
     */
    public static void main(String[] args)
    {
        welcome();
        setup();
        run();
        exit();
    }

    private static void run()
    {
        try
        {
            while (repeat == true) {
                System.out.print("What would you like to do?: " + MENU);

                input = KB.nextLine().toLowerCase();

                switch (input) {
                    case "0":
                    case "test":
                        Two_Dimensional_World world = new Two_Dimensional_World();

                        Tile[][] inGrid = new Tile[INITIAL_LEN_WID][INITIAL_LEN_WID];

                        for(int x = 0; x < inGrid.length; x++)
                        {
                            for(int y = 0; y < inGrid[x].length; y++)
                            {
                                inGrid[x][y] = new Tile();
                            }
                        }

                        //draw top wall
                        for(int x = 0; x < INITIAL_LEN_WID; x++)
                        {
                            //inGrid[0][x] = new Tile(TileType.HORIZ_WALL);
                            inGrid[0][x].setType(TileType.HORIZ_WALL);
                        }

                        //draw bottom wall
                        for(int x = 0; x < INITIAL_LEN_WID; x++)
                        {
                            //inGrid[INITIAL_LEN_WID - 1][x] = new Tile(TileType.HORIZ_WALL);
                            inGrid[INITIAL_LEN_WID - 1][x].setType(TileType.HORIZ_WALL);
                        }

                        //draw left wall
                        for(int x = 0; x < INITIAL_LEN_WID; x++)
                        {
                            //inGrid[x][0] = new Tile(TileType.VERT_WALL);
                            inGrid[x][0].setType(TileType.VERT_WALL);
                        }

                        //draw right wall
                        for(int x = 0; x < INITIAL_LEN_WID; x++)
                        {
                            //inGrid[x][INITIAL_LEN_WID - 1] = new Tile(TileType.VERT_WALL);
                            inGrid[x][INITIAL_LEN_WID - 1].setType(TileType.VERT_WALL);
                        }

                        world.setGrid(inGrid);

                        String out = world.build();

                        System.out.print(CLEAR +
                                            out);

                        KB.nextLine();

                        System.out.print(CLEAR);

                        run();
                    break;
                    case "1":
                    case "up":
                    case "north":
                        notImplemented();
                        break;
                    case "2":
                    case "down":
                    case "south":
                        notImplemented();
                        break;
                    case "3":
                    case "left":
                    case "west":
                        notImplemented();
                        break;
                    case "4":
                    case "right":
                    case "east":
                        notImplemented();
                        break;
                    case "5":
                    case "clear":
                    case "clear screen":
                    case "clr":
                    case "clr screen":
                    case "clear scrn":
                    case "clr scrn":
                        clearScreen();
                        break;
                    case "6":
                    case "status":
                    case "stat":
                    case "stats":
                    case "check stat":
                    case "check stats":
                    case "statistic":
                    case "statistics":
                    case "check status":
                        //out = player0Stat.toString();

                        out = " ";

                        System.out.println(out + "\n" + CONTINUE + "\n\n");

                        KB.nextLine();
                        break;
                    case "7":
                    case "exit":
                    case "leave":
                    case "exit game":
                    case "leave game":
                        repeat = false;
                        break;
                    case "8":
                    case "about":
                    case "about game":
                        about();
                        break;
                    default:
                        error();
                        break;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException exception)
        {
            System.out.println("\n\n" + BORDER +
                    "An array index was invalid!!\n\n" + BORDER + "\n\n" +
                    exception.getMessage() + "\n\n" + BORDER + "\n\n" +
                    exception.toString() + "\n\n" + BORDER + "\n\n");

            exception.printStackTrace();
        }
        catch(ArithmeticException exception)
        {
            System.out.println("\n\n" + BORDER +
                    "Something arithmetic related went wrong!!!!\n\n" + BORDER + "\n\n" +
                    exception.getMessage() + "\n\n" + BORDER + "\n\n" +
                    exception.toString() + "\n\n" + BORDER + "\n\n");

            exception.printStackTrace();
        }
        catch(SecurityException exception)
        {
            System.out.println("\n\n" + BORDER +
                    "There was a security issue\n\n" + BORDER + "\n\n" +
                    exception.getMessage() + "\n\n" + BORDER + "\n\n" +
                    exception.toString() + "\n\n" + BORDER + "\n\n");

            exception.printStackTrace();
        }
        catch (Exception exception)
        {
            System.out.println("\n\n" + BORDER +
                    "An unknown error occurred.\n\n" + BORDER + "\n\n" +
                    exception.getMessage() + "\n\n" + BORDER + "\n\n" +
                    exception.toString() + "\n\n" + BORDER + "\n\n");

            exception.printStackTrace();
        }
        finally
        {
            System.out.println("\n\n"  + "Would you like to exit the program? <<YES or NO>>" + "\n\n");

            input = KB.nextLine().toLowerCase();

            charInput = input.charAt(0);

            if(charInput == 'y')
            {
                exit();
            }
            else
            {
                System.out.println("\n\nRepeating program despite errors...\n" + CONTINUE);

                KB.nextLine();
            }
        }
    }

    private static void about()
    {
        System.out.print(ABOUT_MESSAGE + "\n" + CONTINUE);
        KB.nextLine();
        System.out.println(SKIP_LINES);
    }

    private static void clearScreen()
    {
        System.out.print(CLEAR);
    }

    private static void notImplemented()
    {
        System.out.print(NOT_IMPLEMENTED_MESSAGE + "\n" + CONTINUE);
        KB.nextLine();
        System.out.println(SKIP_LINES);
    }

    private static void exit()
    {
        System.out.print("\n" + GOODBYE_MESSAGE + "\n" + CONTINUE);
        KB.nextLine();
        System.exit(0);
    }

    private static void welcome()
    {
        System.out.print(WELCOME_MESSAGE + "\n" + CONTINUE);
        KB.nextLine();
        System.out.println(SKIP_LINES);
    }

    private static void error()
    {
        System.out.print(ERROR + "\n" + CONTINUE);
        KB.nextLine();
        System.out.println(SKIP_LINES);
    }

    private static void setup()
    {
        playerWeapons[0] = new Weapon();

        players[0] = new Player();

        monsterWeapons[0] = new Weapon();
        monsterWeapons[1] = new Weapon();

        monsters[0] = new Monster();
        monsters[1] = new Monster(false, MonsterType.NORMAL, monsterWeapons[0]);
        monsters[2] = new Monster(true, MonsterType.NORMAL, monsterWeapons[1]);
    }
}
