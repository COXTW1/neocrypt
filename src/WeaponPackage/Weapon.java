package WeaponPackage;

import EntityPackage.*;
import EntityPackage.Enum.*;
import GamePackage.*;
import GamePackage.Enum.*;
import GamePackage.Stat.*;
import ItemPackage.*;
import ItemPackage.Enum.*;
import WeaponPackage.*;
import WeaponPackage.Enum.*;

//add abstract
public class Weapon
{
    //private WeaponType type;
    private WeaponType type;
    private WeaponPackage.Enum.WeaponType type1;

    public Weapon()
    {
        setType(WeaponType.NONE);
    }

    public Weapon(Weapon weapon)
    {
        setType(weapon.getType());
    }

    public WeaponType getType()
    {
        return type;
    }

    public void setType(WeaponType type)
    {
        this.type = type;
    }
}
