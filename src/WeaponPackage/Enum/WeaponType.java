package WeaponPackage.Enum;

public enum WeaponType
{
    NONE,
    STICK,
    SWORD,
    ROCK
}
